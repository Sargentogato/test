console.log('Expect does nothing when the actual value and expected value are the same')
expect(true).toBe(true);

console.log('Expect throws an error when actual and expected value doens´t match')
try {
  expect(false).toBe(true);
  throw new Error();
} catch (error) {
  if (!(error instanceof ActualAndExpectedValueDoNoTMatchError)) {
    throw error;
  }
}

console.log('Function it executes a test')
let testWasExecuted = false

it('do something', () => {testWasExecuted = true})

expect(testWasExecuted).toBe(true)

it('Function describe a test suit', () => {
  let testSuitWasExecuted = false

  describe('subject under test', () => {testSuitWasExecuted = true})

  expect(testSuitWasExecuted).toBe(true)
})
