class ActualAndExpectedValueDoNoTMatchError extends Error {}

class Expect {
  constructor(actual) {
    this.actual = actual;
  }

  toBe(result) {
    if (this.actual !== result) {
      throw new ActualAndExpectedValueDoNoTMatchError();
    }
  }
}

function describe (subjectUnderTest, testSuit) {
  console.log(subjectUnderTest)
  testSuit()
}

function it (description, test) {
  console.log(' ', description)
  test()
}

function expect(actual) {
  return new Expect(actual);
}

